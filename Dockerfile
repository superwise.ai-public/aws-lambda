FROM public.ecr.aws/lambda/python:3.8

# Copy function code
COPY lambda_function.py ${LAMBDA_TASK_ROOT}

# Install the function's dependencies using file requirements.txt
# from your project folder.
COPY requirements.txt  .

RUN yum -y install gcc gcc-c++

RUN pip3 install --upgrade pip wheel setuptools
RUN pip3 install --no-cache-dir -r requirements.txt --target "${LAMBDA_TASK_ROOT}"

# Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
CMD ["lambda_function.lambda_handler"]