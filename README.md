# Superwise.ai Python SDK - AWS Lambda integration

### A python SDK to integrate with superwise.ai via AWS lambda function

In order to use this library you need:

- AWS private ECR

### Install

1. Build your Docker image using the following command.
```
docker build -t superwise-lambda . 
```
2. After the build completes, tag your image so you can push the image to this repository
```
docker tag superwise-lambda:latest <replace-with-your-privte-aws-ecr-id>/superwise-lambda:latest
```
3. Run the following command to push this image to your newly created AWS repository:
```
docker push <replace-with-your-privte-aws-ecr-id>/superwise-lambda:latest
```
4. Create lambda function based on the Container you just uploaded (read more about how to config your lambda function [here](https://docs.superwise.ai/docs/aws-lambda-example) - start from step 3)
    
AWS reference: https://docs.aws.amazon.com/lambda/latest/dg/with-s3-example.html
    

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).


For getting started, HOWTOs, jupyter notebooks examples etc, use Superwise documentation portal:  https://docs.superwise.ai/


## Licence
The Superwise.ai SDK released under MIT licence (See LICENSE file)
